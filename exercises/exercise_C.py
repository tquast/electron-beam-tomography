#Particle Physics 1, Electron Tomography Exercise - Part C
#Author: Thorben Quast, quast@physik.rwth-aachen.de, February 2019

import ROOT
ROOT.gROOT.SetBatch(True)
from configuration import data_input_directory, output_directory
from helpers import repeatedGausFit

NMimosa_Layers = 6
range_min = -0.04 	#mm
range_max = 0.04 	#mm

#1. Read from the TFile from exercise number B:
infile = ROOT.TFile("%s/solutionB.root" % output_directory, "READ")
residuals_x_aligned = {}
residuals_y_aligned = {}
for ml in range(1, NMimosa_Layers+1):
	residuals_x_aligned[ml] = infile.Get("residuals_x_aligned_MIMOSA%s"%ml)
	###########     CODE HERE     ######################


	#read the y-residuals from the ROOT TFile


	###########    END            ######################


###########     CODE HERE     ######################


#zoom into the x-axis of the the residual histograms 


###########    END            ######################

	
	

#3. perform gaussian fits and save the parameters
output_table = []
gaus_fit = ROOT.TF1("gaus", "gaus", range_min, range_max)
for ml in range(1, NMimosa_Layers+1):
	entry = [ml]
	###########     CODE HERE     ######################


	#perform a repeated gaussian fit to the residual-x histogram, look at the repeatedGausFit function in helpers.py for its signature


	###########    END            ######################
	entry.append(1000*gaus_fit.GetParameter(2))

	###########     CODE HERE     ######################


	#perform a repeated gaussian fit to the residual-y histogram, look at the repeatedGausFit function in helpers.py for its signature


	###########    END            ######################	
	entry.append(1000*gaus_fit.GetParameter(2))
	output_table.append(entry)

#4. pretty printing of the obtained resolutions
from tabulate import tabulate
print tabulate(output_table, headers = ["MIMOSA", "x-resolution [mum]", "y-resolution [mum]"])