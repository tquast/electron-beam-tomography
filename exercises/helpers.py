import numpy as np
import ROOT
from math import sqrt

class StraightLineFit:
	def __init__(self):
		self.m = None
		self.b = None
		
		self.xi = None
		self.yi = None

	def Fit(self, x, y):
		if len(x) != len(y):
			print "In fitting: Length of x and y arrays must be identical!"
			return

		###########     CODE HERE     ######################


		#We need a linear fit given an array of x- and associated y-values.
		#For that implement the formula for the slope "self.m" and the offset "self.b" which are used in the Evaluate member function.

		###########    END            ######################


	def Evaluate(self, x):
		if self.m is None:
			return None
		else:
			return self.m * x + self.b


#histogram: histogram to be fitted
#gaus: gaussian ROOT.TF1 object
#rangeInSigmaLeft: left-hand range for the fit in units of fitted sigmas
#rangeInSigmaRight: right-hand range for the fit in units of fitted sigmas
def repeatedGausFit(histogram, gaus, rangeInSigmaLeft=1.0, rangeInSigmaRight=2.5):
	gaus.SetRange(histogram.GetXaxis().GetXmin(), histogram.GetXaxis().GetXmax())

	if rangeInSigmaRight==None:
		rangeInSigmaRight = rangeInSigmaLeft

	if histogram.GetEntries()==0:
		return

	ymaximum = histogram.GetMaximum()
	gaus.SetParameter(1, histogram.GetMean())
	gaus.SetParameter(2, histogram.GetStdDev())
	gaus.SetLineColor(histogram.GetLineColor())
	gaus.SetParLimits(1, histogram.GetMean()-rangeInSigmaLeft*histogram.GetStdDev(), histogram.GetMean()+rangeInSigmaRight*histogram.GetStdDev())

	histogram.Fit(gaus, "QRN")
	for f_i in range(9):
		mu_tmp = gaus.GetParameter(1)
		sigma_tmp = gaus.GetParameter(2)
		gaus.SetParLimits(1, mu_tmp-rangeInSigmaLeft*sigma_tmp, mu_tmp+rangeInSigmaRight*sigma_tmp)
		gaus.SetRange(mu_tmp-rangeInSigmaLeft*sigma_tmp, mu_tmp+rangeInSigmaRight*sigma_tmp)
		gaus.SetParameter(1, mu_tmp)
		gaus.SetParameter(2, sigma_tmp)
		histogram.Fit(gaus, "QRN")
	histogram.Fit(gaus, "QR")



def computeNBins(NEntries, range_minx, range_maxx, range_miny, range_maxy):
  NEntriesMin = 900
  deltay = 1.*(range_maxy-range_miny)/(range_maxx-range_minx)
  NBins = 1.*sqrt(NEntries/NEntriesMin/deltay)
  NBinsX = int(NBins)
  NBinsY = int(NBins * deltay)
  return NBinsX, NBinsY


def setPlainStyle(NCont=500, style="mcStyle"):
  ROOT.gStyle.SetOptStat(0);
  ROOT.gROOT.SetStyle(style)
  NRGBs = 6
  stops = np.array([0.0, 0.1, 0.30, 0.45, 0.50, 0.8])
  red= np.array([1.0,  0.00, 0.00, 1.0, 1.0, 0.])
  green= np.array([1.0, 1.0, 0.11, 0.0, 0.0, 0.])
  blue= np.array([0.0,  0.0, 1.00, 1.0, 0.0, 0.])
  ROOT.TColor.CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont)
  ROOT.gStyle.SetNumberContours(NCont)