#Particle Physics 1, Electron Tomography Exercise - Part B
#Author: Thorben Quast, quast@physik.rwth-aachen.de, February 2019

import numpy as np
import ROOT
ROOT.gROOT.SetBatch(True)
from configuration import data_input_directory, output_directory
from tqdm import tqdm
from helpers import StraightLineFit


NMimosa_Layers = 6
inputFilePath_unaligned = "%s/noAbsorber_default.npy" % data_input_directory
inputFilePath_aligned = "%s/noAbsorber.npy" % data_input_directory
NBins_residuals = 2000
range_min = -1. 	#mm
range_max = 1. 	#mm


#1. loading the data
inp_data = np.load(inputFilePath_unaligned)
xi_unaligned = inp_data.item().get("x")		#the unit is mm
yi_unaligned = inp_data.item().get("y")		#the unit is mm
zi_unaligned = inp_data.item().get("z")		#the unit is mm

inp_data = np.load(inputFilePath_aligned)
xi_aligned = inp_data.item().get("x")		#the unit is mm
yi_aligned = inp_data.item().get("y")		#the unit is mm
zi_aligned = inp_data.item().get("z")		#the unit is mm


NTracks = len(xi_aligned)
print "Number of tracks in the data file:",NTracks


#2. preparing the residual histograms for the unaligned data file
residuals_x_unaligned = {}
residuals_y_unaligned = {}
for ml in range(1, NMimosa_Layers+1):
	residuals_x_unaligned[ml] = ROOT.TH1F("residuals_x_unaligned_MIMOSA%s"%ml, "residuals_x_unaligned_MIMOSA%s"%ml, NBins_residuals, range_min, range_max) 
	residuals_y_unaligned[ml] = ROOT.TH1F("residuals_y_unaligned_MIMOSA%s"%ml, "residuals_y_unaligned_MIMOSA%s"%ml, NBins_residuals, range_min, range_max) 


#3. filling the residual histograms from the unaligned data 
print "Track loop for the unaligned telescope configuration..."
for ntrack in tqdm(range(NTracks), unit="tracks"):
	#compute the reference track given all six planes
	
	###########  Before:  CODE IN helpers.py  ##########


	#implementation of the fitting in helpers.py

	
	###########    END            ######################

	ref_track_x = StraightLineFit()
	ref_track_x.Fit(zi_unaligned[ntrack], xi_unaligned[ntrack])
	for ml in range(1, NMimosa_Layers+1):
		residuals_x_unaligned[ml].Fill(xi_unaligned[ntrack][ml-1]-ref_track_x.Evaluate(zi_unaligned[ntrack][ml-1]))

	###########     CODE HERE     ######################


	#repeat residual computation and storage for the y-coordinate


	###########    END            ######################



#4. Repeat with aligned data
residuals_x_aligned = {}
residuals_y_aligned = {}
for ml in range(1, NMimosa_Layers+1):
	residuals_x_aligned[ml] = ROOT.TH1F("residuals_x_aligned_MIMOSA%s"%ml, "residuals_x_aligned_MIMOSA%s"%ml, NBins_residuals, range_min, range_max) 
	residuals_y_aligned[ml] = ROOT.TH1F("residuals_y_aligned_MIMOSA%s"%ml, "residuals_y_aligned_MIMOSA%s"%ml, NBins_residuals, range_min, range_max) 

###########     CODE HERE     ######################


#compute residuals (x and y) for the aligned data file


###########    END            ######################



#5. Plotting of the residual distributions into pdfs
canvas = ROOT.TCanvas("canvas_x", "canvas_x", 3*800, 2*600)
canvas.Divide(3, 2)
for ml in range(1, NMimosa_Layers+1):
	canvas.cd(ml)
	residuals_x_aligned[ml].SetLineWidth(2)
	residuals_x_aligned[ml].SetLineColor(ROOT.kRed+1)		#aligned residual distribution is red
	residuals_x_aligned[ml].GetXaxis().SetTitle("Residual-x[mm]")
	residuals_x_aligned[ml].GetYaxis().SetTitle("N_{tracks}")
	residuals_x_aligned[ml].SetStats(False)
	residuals_x_aligned[ml].Draw()
	residuals_x_unaligned[ml].SetLineWidth(2)
	residuals_x_unaligned[ml].SetStats(False)
	residuals_x_unaligned[ml].Draw("SAME")
canvas.Print("%s/solutionB_residualsX.pdf" % output_directory)


###########     CODE HERE     ######################


#draw & print the residual distributions (aligned and unaligned) for the y-coordinate


###########    END            ######################



#6. Saving the residual histograms into a ROOT TFile
outfile = ROOT.TFile("%s/solutionB.root" % output_directory, "RECREATE")
for ml in range(1, NMimosa_Layers+1):
	residuals_x_unaligned[ml].Write()
	residuals_y_unaligned[ml].Write()
	residuals_x_aligned[ml].Write()
	residuals_y_aligned[ml].Write()