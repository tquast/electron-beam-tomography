#Particle Physics 1, Electron Tomography Exercise - Part G
#Author: Thorben Quast, quast@physik.rwth-aachen.de, February 2019

import numpy as np
import ROOT
ROOT.gROOT.SetBatch(True)
from configuration import data_input_directory, output_directory
from helpers import computeNBins, setPlainStyle


range_minx = -8
range_maxx = 7.5
range_miny = -4
range_maxy = 5

runs = [742,743,744,745,746,747,748,749,750,751,752,753,754,755,756,757,758,759,760,761,762,763,764,765,766,767,768,769,770,771,772,773,774,775,776,777,778,779,780,781,782,783,784,785]

setPlainStyle(500, "mcStyle")
#reading from the TTree
tree = ROOT.TChain('kinkAngles','kinkAngles')
for index, run in enumerate(runs):
	tree.Add("%s/kinkAngleTTrees/PCB_kinkAngles_run%06d.root"%(data_input_directory, run))
	print "Adding Run", run
#print the content of the tree
print "TTree content:"
tree.Print()  

#set up the histograms like in exercise E
NEntries = tree.GetEntries()
NBinsX, NBinsY = computeNBins(NEntries, range_minx, range_maxx, range_miny, range_maxy)
print NEntries, NBinsX, NBinsY
full_occupancy = ROOT.TH2F("occupancy_DUT1", "occupancy_DUT1", NBinsX, range_minx, range_maxx, NBinsY, range_miny, range_maxy)
full_occupancy_angleweighted = ROOT.TH2F("occupancy_DUT1_angleweighted", "occupancy_DUT1_angleweighted", NBinsX, range_minx, range_maxx, NBinsY, range_miny, range_maxy)

###########     CODE HERE     ######################


#Project the relevant content of the TTree into the 2d histograms.
#see: https://root.cern.ch/root/html524/TTree.html#TTree:Project

###########    END            ######################

divided = full_occupancy_angleweighted
divided.SetStats(False)
divided.GetXaxis().SetTitle("x [mm]")
divided.GetYaxis().SetTitle("y [mm]")
divided.Divide(full_occupancy)
divided.SetTitle("PCB tomography, N_{tracks}: %i"%full_occupancy.GetEntries())
divided.SetName("full_scattering")
divided.GetZaxis().SetRangeUser(0.35, 1.8)
divided.GetZaxis().SetTitleOffset(0.7)
divided.GetZaxis().SetTitle("<k> [mrad]")  
canvas = ROOT.TCanvas("canvas", "canvas", 1200, 800)
divided.Draw("COLZ")
canvas.Print("%s/solutionG_fullStatistics.png" % output_directory)