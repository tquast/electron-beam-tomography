#Particle Physics 1, Electron Tomography Exercise - Part D
#Author: Thorben Quast, quast@physik.rwth-aachen.de, February 2019

import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptFit()
from configuration import data_input_directory, output_directory
from math import sqrt, log

#objects to be filled:
X0Thicknesses = {
	"Aluminium_0Point4mm": 0,
	"Aluminium_0Point9mm": 0,
	"Aluminium_1Point0mm": 0,
	"Aluminium_1Point2mm": 0,
	"Copper_1Point0mm": 0
}
MeanKinkAngle = {
	"Aluminium_0Point4mm": 0,
	"Aluminium_0Point9mm": 0,
	"Aluminium_1Point0mm": 0,
	"Aluminium_1Point2mm": 0,
	"Copper_1Point0mm": 0
}
MeanKinkAngleError = {
	"Aluminium_0Point4mm": 0,
	"Aluminium_0Point9mm": 0,
	"Aluminium_1Point0mm": 0,
	"Aluminium_1Point2mm": 0,
	"Copper_1Point0mm": 0
}

#1. open the file from exercise C and obtain the mean kink angle values plus their error
infile = ROOT.TFile("%s/solutionD.root" % output_directory, "READ")
for plate in MeanKinkAngle:
	h1_kinkangle = infile.Get(plate)
	MeanKinkAngle[plate] = h1_kinkangle.GetMean()
	MeanKinkAngleError[plate] = h1_kinkangle.GetRMS() / sqrt(2.*h1_kinkangle.GetEntries())

infile.Close()


###########     CODE HERE     ######################


#2. write down the equivalent thicknesses in X0 for each plate, e.g.
#X0Thicknesses["Aluminium_0Point4mm"] = 1.2345


###########    END            ######################



#3. Fill a TGraphErrors
calibration_graph = ROOT.TGraphErrors()
for point_index, plate in enumerate(sorted(X0Thicknesses)):
	calibration_graph.SetPoint(point_index, X0Thicknesses[plate], MeanKinkAngle[plate])
	calibration_graph.SetPointError(point_index, 0., MeanKinkAngleError[plate])
calibration_graph.SetMarkerStyle(22)
calibration_graph.SetMarkerSize(2)
calibration_graph.GetXaxis().SetTitle("Plate thickness [X_{0}]")
calibration_graph.GetYaxis().SetTitle("Mean kink angle [mrad]")
calibration_graph.SetTitle("Mean kink angle calibration")


###########     CODE HERE     ######################


#4. Define and fit a suitable function for the calibration of the mean kink angle - material thickness

#technical hint:
#def multiplescattering_width(x, par):
#	if x[0]<=0:
#		ROOT.TF1.RejectPoint()
#		return 0
	#expected relationship
#	return SOMETHING

#fit_func = ROOT.TF1("scale_func",multiplescattering_width, 0., max([X0Thicknesses[p] for p in X0Thicknesses]), NUMBEROFPARAMETERS)
#calibration_graph.Fit(fit_func)


###########    END            ######################



#5. Saving the output
canvas = ROOT.TCanvas("canvas", "canvas", 1200, 800)
calibration_graph.Draw("AP")
#moves the statbox down
ROOT.gStyle.SetStatY(0.5)
canvas.Print("%s/solutionE_calibration.pdf" % output_directory)