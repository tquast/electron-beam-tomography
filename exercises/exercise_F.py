#Particle Physics 1, Electron Tomography Exercise - Part F
#Author: Thorben Quast, quast@physik.rwth-aachen.de, February 2019

import numpy as np
import ROOT
ROOT.gROOT.SetBatch(True)
from configuration import data_input_directory, output_directory
from tqdm import tqdm
from math import sqrt, acos
from helpers import StraightLineFit, setPlainStyle


z_DUT = 370.		#DUT is placed at 370. mm along the z-axis

range_minx = -8.
range_maxx = 7.5
range_miny = -4.
range_maxy = 5.



###########     CODE HERE     ######################


#choose the binning of the tomography image
#NBinsX = 195
#NBinsY = 113

#choose how many tracks are to be fitted for the tomography image, e.g.
#maxNTracks = int(1e5)			#full available statistic: "-1"


###########    END            ######################


#1. loading the data
print "Loading the data file..."
inputFilePath = "%s/PCBRegion.npy" % data_input_directory
inp_data = np.load(inputFilePath)
xi = inp_data.item().get("x")   #the unit is mm
yi = inp_data.item().get("y")   #the unit is mm
zi = inp_data.item().get("z")   #the unit is mm

NTracks = len(xi)
print "Number of tracks in the data file:",NTracks
if maxNTracks>0:
	NTracks = min(maxNTracks, NTracks)


#2. prepare the histograms
h2_occupancy = ROOT.TH2F("occupancy", "occupancy", NBinsX, range_minx, range_maxx, NBinsY, range_miny, range_maxy)
h2_occupancy_angleweighted = ROOT.TH2F("occupancy_angleweighted", "occupancy_angleweighted", NBinsX, range_minx, range_maxx, NBinsY, range_miny, range_maxy)

#3. fitting the tracks and computing the kink angle
print "Track loop..."
for ntrack in tqdm(range(NTracks), unit="tracks"):
	
	
	###########     CODE HERE     ######################


	#analogous to exercise D: fit the triplet tracks and compute the kink angle from the scalar product of the tracks


	###########    END            ######################



	###########     CODE HERE     ######################


	#compute the impact position at the device under test
	#impactX = 
	#impactY = 


	###########    END            ######################

	h2_occupancy.Fill(impactX, impactY)
	h2_occupancy_angleweighted.Fill(impactX, impactY, kink_angle)


#4. drawing of the occupancy
h2_occupancy.GetXaxis().SetTitle("impact x [mm]")
h2_occupancy.GetYaxis().SetTitle("impact y [mm]")
h2_occupancy.GetZaxis().SetTitle("N_{tracks}")
h2_occupancy.SetTitle("Electron track occupancy at DUT")
h2_occupancy.SetStats(False)
canvas_occupancy = ROOT.TCanvas("occupancy", "occupancy", 1200, 800)
h2_occupancy.Draw("COLZ")
canvas_occupancy.Print("%s/solutionF_occupancy.pdf" % output_directory)


###########     CODE HERE     ######################


#compute the average kink angle in bins of the impact position by dividing both 2-D histograms


###########    END            ######################
#5. compute the mean kink angle per impact position and draw it
h2_occupancy_angleweighted.GetXaxis().SetTitle("impact x [mm]")
h2_occupancy_angleweighted.GetYaxis().SetTitle("impact y [mm]")
h2_occupancy_angleweighted.GetZaxis().SetTitle("mean kink angle [mrad]")
h2_occupancy_angleweighted.GetZaxis().SetRangeUser(0.35, 1.8)
h2_occupancy_angleweighted.GetZaxis().SetTitleOffset(0.7)
h2_occupancy_angleweighted.SetTitle("mean kink angle")
h2_occupancy_angleweighted.SetStats(False)
setPlainStyle(500, "mcStyle")
canvas_meankink = ROOT.TCanvas("meankink", "meankink", 1200, 800)
h2_occupancy_angleweighted.Draw("COLZ")
canvas_meankink.Print("%s/solutionF_meanKink.pdf" % output_directory)