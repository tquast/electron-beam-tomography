#Particle Physics 1, Electron Tomography Exercise - Part A
#Author: Thorben Quast, quast@physik.rwth-aachen.de, February 2019

import numpy as np
import ROOT
ROOT.gROOT.SetBatch(True)
from configuration import data_input_directory, output_directory
from tqdm import tqdm


inputFilePath_raw = "%s/raw_pixels.npy" % data_input_directory
#MIMOSA dimensions:
N_rows = 576
N_columns = 1152

#1. loading the data
inp_data = np.load(inputFilePath_raw)
raw_pixels_plane = {
	1: inp_data.item().get("Pixels_plane1"),
	2: inp_data.item().get("Pixels_plane2"),
	3: inp_data.item().get("Pixels_plane3"),
	4: inp_data.item().get("Pixels_plane4"),
	5: inp_data.item().get("Pixels_plane5"),
	6: inp_data.item().get("Pixels_plane6")
}


#2. prepare the number of hits histogram
h1_nhits = {}
for plane in range(1, 7):
	h1_nhits[plane] = ROOT.TH1I("nhits_plane%s"%plane, "nhits_plane%s"%plane, 41, -0.5, 40.5)
	h1_nhits[plane].GetXaxis().SetTitle("Number of hits")
	h1_nhits[plane].GetYaxis().SetTitle("N_{events}")


#3. loop over all planes and fill the number of hits
for plane in range(1, 7):
	print "Event loop for plane",plane,"..."
	for nevent in tqdm(range(len(raw_pixels_plane[plane]))):

		###########     CODE HERE     ######################
		

		#obtain the number of pixels of each plane in a given readout (=event) and fill it into the appropriate histogram


		###########    END            ######################


#4. printing
canvas_nhits = ROOT.TCanvas("canvas_nhits", "canvas_nhits", 2400, 1200)
canvas_nhits.Divide(3, 2)
for plane in range(1, 7):
	canvas_nhits.cd(plane)
	h1_nhits[plane].Draw()
canvas_nhits.Print("%s/solutionA_nhits.pdf" % output_directory)


#5. correlation plot for MIMOSA planes 1 & 2
h2_correlation_1_2_xx = ROOT.TH2I("h2_correlation_1_2_xx", "h2_correlation_1_2_xx", N_columns, 0.5, N_columns+0.5, N_columns, 0.5, N_columns+0.5)
h2_correlation_1_2_xx.SetStats(False)
h2_correlation_1_2_xx.GetXaxis().SetTitle("column-index plane 1")
h2_correlation_1_2_xx.GetYaxis().SetTitle("column-index plane 2")

###########     CODE HERE     ######################


#in a similar fashion, allocate the 2-D histograms for the y-y, x-y and y-x correlations of the first two telescope planes
#x = column
#y = row


###########    END            ######################


#6. filling them
print "Event loop..."
for nevent in tqdm(range(len(raw_pixels_plane[1])), unit="ev"):
	#inside event loop
	for npixel_1 in range(len(raw_pixels_plane[1][nevent])):
		#inside loop over all pixels of plane 1
		for npixel_2 in range(len(raw_pixels_plane[2][nevent])):
			#inside loop over all pixels of plane 2
			column_1 = raw_pixels_plane[1][nevent][npixel_1][0]

			###########     CODE HERE     ######################


			#similarly, obtain the missing column and row indexes of planes 1 and 2 and fill the relevant information into the correlation histograms
			
			
			###########    END            ######################			


#7. printing
canvas_correlations = ROOT.TCanvas("canvas_correlations", "canvas_correlations", 1600, 1200)
canvas_correlations.Divide(2, 2)
canvas_correlations.cd(1)
ROOT.gPad.SetLogz(True)
h2_correlation_1_2_xx.Draw("COLZ")
canvas_correlations.cd(2)

###########     CODE HERE     ######################


#draw the remaining histograms


###########    END            ######################	

canvas_correlations.Print("%s/solutionA_correlations.png" % output_directory)