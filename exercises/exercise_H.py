#Particle Physics 1, Electron Tomography Exercise - Part A
#Author: Thorben Quast, quast@physik.rwth-aachen.de, April 2019

import numpy as np
import ROOT
ROOT.gROOT.SetBatch(True)
from configuration import data_input_directory, output_directory
from tqdm import tqdm

from helpers import repeatedGausFit
colorMap = {
	"noAl": ROOT.kRed+1,
	"withAl": ROOT.kBlack
}


#1. define the datasets
inputData = {}
#configure the 50 GeV electron datasets
inputData[50] = {}
inputData[50]["noAl"] = "config101_e+_50GeV_step20_t0.npy"
inputData[50]["withAl"] = "config102_e+_50GeV_step20_t0.npy"
#load the 100 GeV electron datasets
inputData[100] = {}
inputData[100]["noAl"] = "config101_e+_100GeV_step20_t0.npy"
inputData[100]["withAl"] = "config102_e+_100GeV_step20_t0.npy"

#book keeping dictionaries of the energy spectra (TH1) and the gaussian fits (TF1)
h1_energy_spectra = {}
f1_gaus_fits = {}

#loop over the datasets and compute the energy sum spectra
for energy in inputData:
	h1_energy_spectra[energy] = {}
	f1_gaus_fits[energy] = {}
	for specification in inputData[energy]:

		#2. load the data
		inpath = "%s/%s" % (data_input_directory, inputData[energy][specification])
		print "Loading",inpath
		data = np.load(inpath)
		N_Events = len(data)
		print N_Events, "showers loaded"

		#3. initialise the energy spectrum and the gaussian function
		h1_name = h1_title = "h1_energy_spectrum_%sGeV_%s" % (energy, specification)
		h1_energy_spectra[energy][specification] = ROOT.TH1F(h1_name, h1_title, 200, 300., 1000.)
		f1_name = "f1_gaus_energy_spectrum_%sGeV_%s" % (energy, specification)
		f1_gaus_fits[energy][specification] = ROOT.TF1(f1_name, "gaus", 0., 1000.)

		#4. fill the spectrum with the per-event energy sum in MeV
		for n_event in tqdm(range(N_Events)):
			NHits = len(data[n_event]["Edep_keV"])
			ESum = 0
			###########     CODE HERE     ######################

			#compute the energy sum

			###########    END            ######################			
			h1_energy_spectra[energy][specification].Fill(ESum)



		###########     CODE HERE     ######################
		
		#5. perform the gaussian fit
		

		###########    END            ######################


		#Setting some visual attributes:
		h1_energy_spectra[energy][specification].SetLineColor(colorMap[specification])
		f1_gaus_fits[energy][specification].SetLineColor(colorMap[specification])
		h1_energy_spectra[energy][specification].SetStats(False)


#6. Drawing the spectra
canvas_energy_spectra = ROOT.TCanvas("canvas_energy_spectra", "canvas_energy_spectra", 2400, 1000)
canvas_energy_spectra.Divide(2)

legends = {}
for canvas_index, energy in enumerate(sorted(inputData)):
	canvas_energy_spectra.cd(canvas_index+1)
	h1_energy_spectra[energy]["noAl"].SetTitle("")
	h1_energy_spectra[energy]["noAl"].GetXaxis().SetTitle("Sampled energy sum of a %s GeV electron shower [MeV]" % energy)
	h1_energy_spectra[energy]["noAl"].GetYaxis().SetTitleOffset(1.3 * h1_energy_spectra[energy]["noAl"].GetYaxis().GetTitleOffset())
	h1_energy_spectra[energy]["noAl"].GetYaxis().SetTitle("Frequency")
	h1_energy_spectra[energy]["noAl"].Draw("HIST")
	h1_energy_spectra[energy]["withAl"].Draw("HISTSAME")
	f1_gaus_fits[energy]["noAl"].Draw("SAME")
	f1_gaus_fits[energy]["withAl"].Draw("SAME")
	
	legends[canvas_index] = ROOT.TLegend(0.10, 0.90, 0.92, 0.99)
	legends[canvas_index].SetNColumns(2)
	legends[canvas_index].AddEntry(h1_energy_spectra[energy]["noAl"], "No Aluminium: #mu = %s MeV" % round(f1_gaus_fits[energy]["noAl"].GetParameter(1), 1))
	legends[canvas_index].AddEntry(h1_energy_spectra[energy]["withAl"], "With Aluminium: #mu = %s MeV" % round(f1_gaus_fits[energy]["withAl"].GetParameter(1), 1))
	legends[canvas_index].Draw()


canvas_energy_spectra.Print("%s/solutionH_caloPerformance.pdf" % output_directory)