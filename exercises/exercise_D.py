#Particle Physics 1, Electron Tomography Exercise - Part D
#Author: Thorben Quast, quast@physik.rwth-aachen.de, February 2019

import numpy as np
import ROOT
ROOT.gROOT.SetBatch(True)
from configuration import data_input_directory, output_directory
from tqdm import tqdm
from helpers import StraightLineFit
from math import sqrt, acos

maxNTracks = 100000
NBins = 1000
range_min = 0.   #mm
range_max = 10.  #mrad

filenames = []
filenames.append("Aluminium_0Point4mm")
filenames.append("Aluminium_0Point9mm")
filenames.append("Aluminium_1Point0mm")
filenames.append("Aluminium_1Point2mm")
filenames.append("Copper_1Point0mm")


#a dictionary holding the histograms:
h1_mean_angle = {}

#prepare the anvas and the legend
canvas = ROOT.TCanvas("canvas", "canvas", 1200, 800)
legend = ROOT.TLegend(0.6, 0.6, 0.9, 0.9)

#loop over all files
for findex, fname in enumerate(filenames):
  inputFilePath = "%s/%s.npy" % (data_input_directory, fname)
  #1. loading the data
  inp_data = np.load(inputFilePath)
  xi = inp_data.item().get("x")   #the unit is mm
  yi = inp_data.item().get("y")   #the unit is mm
  zi = inp_data.item().get("z")   #the unit is mm

  NTracks = len(xi)
  print "Number of tracks in the data file (",fname,"):",NTracks
  NTracks = min(maxNTracks, NTracks)

  #2. prepare the histograms
  h1_mean_angle[fname] = ROOT.TH1F(fname, fname, NBins, range_min, range_max)

  #3. fitting the tracks and computing the kink angle
  for ntrack in tqdm(range(NTracks), unit="tracks"):
    #compute the reference track given all six planes
    ref_track_x_triplet1 = StraightLineFit()
    ref_track_x_triplet1.Fit(zi[ntrack][0:3], xi[ntrack][0:3])

    ###########     CODE HERE     ######################


    #compute the remaining triplet fits, i.e. for the y-coordinate using planes 1-3 and x/y using planes 4-6


    ###########    END            ######################

    #scalar product formula
    kink_angle = 0
    ###########     CODE HERE     ######################


    #compute the kink_angle given the four slopes "ref_track_x(y)_triplet1(2).m" 
    #provide the value of the kink angle in units of mrad


    ###########    END            ######################
    
    h1_mean_angle[fname].Fill(kink_angle)

  #plotting
  h1_mean_angle[fname].GetXaxis().SetTitle("kink angle [mrad]")
  h1_mean_angle[fname].SetLineWidth(2)
  h1_mean_angle[fname].SetLineColor(ROOT.kBlack+findex)
  h1_mean_angle[fname].SetStats(False)
  if findex==0:
    h1_mean_angle[fname].SetTitle("Kink angle distribution")
    h1_mean_angle[fname].Draw()
  else:
    h1_mean_angle[fname].Draw("SAME")
  legend.AddEntry(h1_mean_angle[fname], fname.replace("Point", ".").replace("_",": "), "l")

#print the canvas and the legend
legend.Draw()
canvas.Print("%s/solutionD_kinkAngles.pdf" % output_directory)

#saving histograms to file for exercise  D
outfile = ROOT.TFile("%s/solutionD.root" % output_directory, "RECREATE")
for fname in filenames:
  h1_mean_angle[fname].Write()
outfile.Close()