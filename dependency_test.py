#Particle Physics 1, Electron Tomography Exercise - Testing availaibility of required software dependencies
#Author: Thorben Quast, quast@physik.rwth-aachen.de, April 2019

import sys

#check for numpy and the correct version
try:
	import numpy
	print "Successful: numpy available"
except:
	sys.exit("ERROR:              numpy is not installed!")
nv = numpy.__version__
if "1.16" in nv:
	print "Successful: numpy Version:", nv
else:
	sys.exit("ERROR:              Numpy version is not 1.16")

#check for ROOT and the correct version
try:
	import ROOT
	print "Successful: ROOT available"
except:
	sys.exit("ERROR:              ROOT is not installed!")
rv = ROOT.gROOT.GetVersion()
if "5." in rv or "6." in rv:
	print "Successful: ROOT Version:", rv
else:
	sys.exit("ERROR:              Incompatible ROOT version")

#check for tabulate
try:
	import tabulate
	print "Successful: tabulate available"
except:
	sys.exit("ERROR:              tabulate is not installed!")


#check for tqdm
try:
	import tqdm
	print "Successful: tqdm available"
except:
	sys.exit("ERROR:              tqdm is not installed!")	


print "--> All necessary dependencies to run this exercises are installed"