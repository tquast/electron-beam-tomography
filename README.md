# Electron Beam Tomography Exercise

Exercise on electron beam tomography exploiting multiple scattering of high energetic electrons.
It makes of DATURA beam telescope data that were recorded at DESY in March 2018 during a test for the CMS HGCal upgrade project.

**Introductory slides**: [Link](http://tquast.web.cern.ch/tquast/HGCal_PCB_Tomography_intro.pdf).

**Lab slides**: [Link](http://tquast.web.cern.ch/tquast/HGCal_PCB_Tomography_slides.pdf).

## Idea
Measure electron tracks in front and behind a material under investigation. The average deflection at the point of this material is related to its thickness.
Original idea: [https://telescopes.desy.de/Material_Budget_Imaging](https://telescopes.desy.de/Material_Budget_Imaging).

## Experimental setup
![](images/setup_photo.jpg)

The DESY test beam line provides beams of electrons with 3GeV /c in momentum. Six silicon-pixel planes ([MIMOSA26](https://twiki.cern.ch/twiki/bin/view/MimosaTelescope/MIMOSA26)) of the DATURA telescope are installed and readout. They serve for electron tracking
in front and behind a material placed in the middle of its 2 arms.
More on the test beam line and the DATURA telescope can be found here: 
- [DESY test beam line](http://particle-physics.desy.de/e252106/)
- [DATURA telescope](http://particle-physics.desy.de/e252106/e252107/e252163/)


# Exercise preparation
Students can use their own laptop to complete this exercise. Apart from cloning the exercise repository, the preparation is twofold: First, the necessary input data need to be downloaded. Second, software dependencies must be installed.

## Cloning the exercise repository
In a terminal execute the following:
* ```git clone https://gitlab.cern.ch/tquast/electron-beam-tomography.git <path-to-the-exercise-code-directory>```
* ```cd <path-to-the-exercise-code-directory>```
* ```git checkout INFIERI2019```

## Data download 
The (telescope) data used for this exercise were taken in the context of a beam test for the CMS HGCal upgrade project at DESY in 2018. Pre-processed samples are provided and can be downloaded from here:
[CERNBOX, total size: 3.9 GB, expiration date: 31 Dec 2025](https://cernbox.cern.ch/index.php/s/aupGSEvxwa2AfEa).


## Software installation
Students can choose from two options: Either all required software is installed independently or a Docker container ("=virtual machine") can be downloaded and used.

### Option 1: User self-responsible 
In order to run the exercises properly, make sure you have access to the data directory, use **python 2.7** and have the following libraries installed:
- numpy v1.16 
- ROOT 5 or 6 ("pyROOT")
- [tabulate](https://pypi.org/project/tabulate/) 
- [tqdm](https://github.com/tqdm/tqdm) 

After successful installation, ensure that the packages are all available within a python session. For that, execute the *dependency_test.py* script inside a terminal.
* ```cd <exercise-repository-directory>```
* ```python dependency_test.py```	
* When all required dependencies are installed, the script will end with *--> All necessary dependencies to run this exercises are installed*.
* Set the absolute paths of the directories for the input data and the exercise outputs in ```exercises/configuration.py```.

### Option 2: Docker container
* Install [Docker](https://docs.docker.com/install/).
* Using the terminal, pull the docker container prepared for this tutorial: ```docker pull thorbenquast/infieri2019_tomographyexercise```.
* Using the terminal, execute the container and mount the folders for the data, the code and outputs of this exercise. This will open a terminal with the correctly configured software environment for this exercise: ```docker run -it -v <path-to-the-exercise-code-directory>:/code -v <path-to-the-data-directory>:/data -v <path-to-the-exercise-output-directory>:/outputs thorbenquast/infieri2019_tomographyexercise```. 
* Convince yourself that the required software is already installed. ```cd /code; python dependency_test.py;```
* The exercise code within this docker container instance can be found in ```/code/exercises```. While the script execution is run within the docker container, the script files can be edited on the host computer in the ```<path-to-the-exercise-code-directory>``` using any editor of preference.

## Exercises
This exercise consists of six mandatory parts and one optional. Their sequence is motivated by a typical workflow implemented in the analysis of beam telescope data.
* **A**: Data quality monitoring during data taking with a pixelated beam telescope which should incorporate time-efficient algorithms with both graspable and human readable output.
* **B**: After implementation of a simple track model, impact positions can be computed and compared to the position of reconstructed clusters. This leads to residual distributions which provide a handle on the detector mis-alignment which is essential to correct for.
* **C**: Alignment-corrected residual distributions serve as estimators for the characteristic pointing resolution of the tracking device.
* **D**: Development the analysis strategy and its application on an intuitive dataset.
* **E**: Calibration of the measured quantity with respect to the targeted one.
* **F**: Perform the analysis/measurement on a limited dataset.
* **G**: Usage of the full recorded dataset.
* **(Optional part) H**: Estimate the impact of this extra material on the calorimetric performance on electromagnetic showers.

All template scripts for each part are located in the ```exercises``` directory. Simply run each of them by executing ```python exercise_<X>.py``` within the ```exercises```directoy.
![](images/setup_sketch.jpg)

### Part A: Data quality monitoring
Each readout of a telescope plane consists of a list of pixels which have recorded signal above a predefined threshold in the event. Hereby, one pixel is characterised by its row and column indexes referred to as "xi", "yi". 
Ideally, only those which have been traversed by a charged particle would be active and consequently end up in the list. However, electronic noise results in fake signals and hence fake hits.
```exercise_A.py``` contains the functionality to read exemplary telescope data. Determine the number of active pixels in each event and plane and estimate their distributions. How many pixels are typically active per readout?
In addition, visualise the correlation of pixel coordinates between the first and second plane. Do so by constructing all possible combinations of (x1,x2), (x1, y2), (y1, y2), (y1, x2) for each event and filling these points into two-dimensional histograms. What can one learn about the setup from these plots?

### Part B: Track model and alignment
In subsequent reconstruction steps, pixels with abnormally high occupancies are identified, considered noisy and rejected from further analysis. Afterwards, neighboring pixels are combined to clusters. Clusters whose physical location is consistent with the trajectory of a charged particle going through it are selected (for an exemplary algorithm, see e.g. [here](http://tquast.web.cern.ch/tquast/NtletTracking.pdf)). In ```exercise_B.py``` two files with trajectory-matched clusters are provided. A cluster is characterised by its coordinate (x, y, z, unit: mm) in a cartesian system. The first file contains the cluster of the mis-aligned system, meaning the orientation and / or reference point of all planes do not coincide with a common reference coordinate system.
Construct straight line trajectories given the clusters from the six planes and compute the deviation from its extra- / interpolation with respect to the measurement (=residual) at each plane. Compute the distribution of the residuals at each plane and for each dimension (6 planes x 2 dimensions = 12 plots). Repeat the same for the second file where alignment corrections are incorporated. Overlay the residual distributions for the unaligned and the aligned data. 
Why is alignment important? 

### Part C: Pointing resolution
Use the residual distributions from part B to estimate the pointing resolution of each plane. The core part of the distribution is well-described by a Gaussian function. Fit them to the data and report the sigma as the estimate for the resolution. Explain why some of the values are lower for some planes than for others. Could it be a systematic bias due to this method?

### Part D: Kink angle for different absorbers
So far, the only material in between the telescope planes has been air. Any effect from multiple scattering was not considered.
In this ```exercise_D.py```, you qualitatively analyse the effect of multiple scattering due to materials of different thickness placed in between the two telescope arms. The chosen materials were alumnium plates of 0.4 - 1.2 mm in thickness as well as one 1.0 mm-thick Copper plate.
Determine the kink ankle between measured electron trajectory from the first three with respect to the trajectory from the last three telescope planes for all events. Compute the distribution of kink angles for each material and visualise them into one graphic. Does the result meet your expectation?

### Part E: Kink angle - thickness calibration
Compute the mean kink angle from the distributions of part D. Draw them as a function of the material thickness in units of radiation lengths. Show that Highland's formula can be applied for modeling the relationship between the mean kink angle and the material thickness and fit it to the data points. Note that the electron beam momentum is 3 GeV /c. What are the limitations of this calibration?

### Part F: Tomography 
For the data used in ```exercise_F.py``` the electron beam was focussed on the highlighted area on a printed circuit board.
![](images/PCB_tomography_focus.jpg)
Following the strategy developed in part D, construct both the kink angle and the impact position on the highlighted area (located at z=370mm along the beam axis) for 100,000 events. Finally, compute and visualise both the electron occupancy at the PCB and the mean kink angle as a function of the impact position. What do you see? Increase the number of processed events (full statistic is approx. 5 million). How does the image change and why?

### Part G: Tomography with full dataset
The full available dataset of more than 75 million electron events has been analysed following the strategy described in part F. Kink angles and impact position coordinates are stored in a ROOT TTree. Its ```Project``` member function allows for efficient projection of its content into predefined histograms. Implement the projection of the TTree into the histograms and compute the mean kink angle as a function of the impact position on the full dataset.

### (Optional) Part H: Impact of additional material on the calorimetric performance.
The tomography of additional areas on the PCB reveals that the equivalent additional thickness of the ROC on this PCB amounts to approximately 2mm of Alumnium.
In order to assess the impact of this additional material on the calorimetric performance, 50 and 100 GeV electron showers are simulated in two HGCal inspired models of an electromagnetic sampling calorimeter [--> Geant4 Simulation](https://github.com/ThorbenQuast/HGCal_TB_Geant4/releases/tag/v0.9_INFIERI2019). While the first one features of 2mm air gaps in between adjacent PCBs, the second one includes 2mm thick 1x1cm2 Alumnium plates within these gaps.
![](images/SimConfigs101_102.png)
Load the provided datasets of calorimeter hits. For each shower determine the sum of their signal in MeV and compute the sampled energy spectrum for each shower energy and model.
Fit gaussian curves to these spectra and use the mu-parameter as estimator for the average response. Plot the obtained spectra at a fixed energy for both configurations in one coordinate system. Should the ROC's material be considered in the detailed simulation? 



## Acknowledgment
![](images/AIDA_2020_acknowledgement_presentations_1.jpg)
